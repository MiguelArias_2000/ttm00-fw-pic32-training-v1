#include <stdint.h>
#include <stdbool.h>
#include <config_bits.h>
#include <plib.h>
#include <proc/p32mx470f512h.h>

#define LED_RED BIT_4
#define LED_GREEN BIT_6
#define LED_YELLOW BIT_7
#define LED_RGB_BLUE BIT_2

#define PBCLK 96000000

#define TMR1_PRESCALE 256
#define TICK_PER_SEC 1000
#define T1_TICK_RATE (PBCLK / (TMR1_PRESCALE * TICK_PER_SEC))

#define BAUD_RATE 115200
#define BAUD_RATE_REGISTER_VALUE ((PBCLK/(16*BAUD_RATE))-1) 

//Main
void gpio_init(void);
void gpio_setup(void);
void timer1_init(void);

//While
void button_polling(void);
void timer1_polling_flag(void);
void LED_heartbeat(uint8_t index_LED);
void uart1_tx_heartbeat(void);
void uart1_rx_polling(void);
bool uart_data_verification(void);
void handle_heartbeat_command(void);