#include <main.h>


uint16_t UART_counter = 0;
uint16_t UART_heartbeat = 1000;
//Position -> LED
//0, 1, 2, 3 -> LED1(Red), LED2(Green), LED3(Yellow), , LED4 (RGB Blue)
uint16_t LEDs_heartbeat_period[4] = {200,500,1000,0};
uint16_t LEDs_counter[4] = {0,0,0,0};

uint8_t uart_rx_data[4] = {0,0,0,0};
uint16_t heartbeat_command_control[4] = {'*', 'R', '1', '#'};

void main(void) 
{
    gpio_init();
    gpio_setup();
    
    //Timer1 Configuration
    //CloseTimer1();
    OpenTimer1(T1_ON | T1_PS_1_256 | T1_SOURCE_INT,
               T1_TICK_RATE);
    
    //UART Communication
    CloseUART1();
    U1RXR = 0b0100;
    RPF0R = 0b0011;
    OpenUART1(UART_EN, 
            UART_TX_ENABLE | UART_RX_ENABLE, 
            BAUD_RATE_REGISTER_VALUE);
    //UART1 Test
    WriteUART1('X');
    
    while(1)
    {
        button_polling();
        timer1_polling_flag();
        int i;
        for (i = 0; i < 3; i++) 
        {
            LED_heartbeat(i);
        }
        uart1_tx_heartbeat();
        uart1_rx_polling();
    }
}

void gpio_init(void)
{
    //Red LED as OUTPUT
    mPORTESetPinsDigitalOut(LED_RED);
    //Green LED as OUTPUT
    mPORTESetPinsDigitalOut(LED_GREEN);
    //Yellow LED as OUTPUT
    mPORTESetPinsDigitalOut(LED_YELLOW);
    //RGB Blue LED as OUTPUT
    mPORTBSetPinsDigitalOut(LED_RGB_BLUE);
    
    //S1 as INPUT
    mPORTDSetPinsAnalogIn(BIT_6);
}

void gpio_setup(void)
{
    //TURN OFF Red LED (LED1)
    mPORTEClearBits(LED_RED);
    //TURN OFF Green LED (LED2)
    mPORTEClearBits(LED_GREEN);
    //TURN OFF Yellow LED (LED3)
    mPORTEClearBits(LED_YELLOW);
    //TURN OFF BLUE RGB LED (LED4)
    mPORTEClearBits(LED_RGB_BLUE);            
}

void button_polling(void)
{
    //if(PORTDbits.RD6 == 0)
    if(mPORTDReadBits(BIT_6))
    {
        //Turn ON RGB Blue
        mPORTBSetBits(LED_RGB_BLUE);        
    }
    else
    {
        //Turn OFF RGB Blue
        mPORTBClearBits(LED_RGB_BLUE);
    }
}

void timer1_polling_flag(void)
{
    //Every 1ms
    if(ReadTimer1() == ReadPeriod1())
    {
        WriteTimer1(0);        
        int i;
        for (i = 0; i < 3; i++) 
        {
            LEDs_counter[i]++;            
        }
        UART_counter++;
    }
}

void LED_heartbeat(uint8_t index_LED)
{
    if(LEDs_counter[index_LED]++ >= LEDs_heartbeat_period[index_LED])
    {
        //Turn ON LED Red
        switch(index_LED)
        {
            //RED
            case 0: 
                mPORTEToggleBits(LED_RED);
            break;
            //GREEN
            case 1:
                mPORTEToggleBits(LED_GREEN);                
            break;
            //YELLOW
            case 2:
                mPORTEToggleBits(LED_YELLOW);
            break;
            //RGB BLUE
            case 3:
                mPORTBToggleBits(LED_RGB_BLUE);
            break;
        }
        LEDs_counter[index_LED] = 0;
    }
    
    if(LEDs_heartbeat_period[index_LED] == 0)
    {
        switch(index_LED)
        {
            //RED
            case 0: 
                mPORTEClearBits(LED_RED);
            break;
            //GREEN
            case 1:
                mPORTEClearBits(LED_GREEN);
            break;
            //YELLOW
            case 2:
                mPORTEClearBits(LED_YELLOW);
            break;
            //RGB BLUE
            case 3:
                mPORTBClearBits(LED_RGB_BLUE);
            break;
        }
    }
}

void uart1_tx_heartbeat(void)
{
    if(UART_counter >= UART_heartbeat)
    {
        //Load LED1 (RED)  period heartbeat to TX register
        WriteUART1((LEDs_heartbeat_period[0]/100)+48);
        WriteUART1((LEDs_heartbeat_period[0]/10) - (LEDs_heartbeat_period[0]/100)*10 +48);
        WriteUART1(0 + 48);
        WriteUART1(10);
        UART_counter = 0;
    }
}

void uart1_rx_polling(void)
{    
    if(IFS1bits.U1RXIF == 1)
    {
        uint8_t index_rx_data;
        for (index_rx_data = 0; index_rx_data < 4; index_rx_data++)
        {
            while (IFS1bits.U1RXIF == 0);
            uart_rx_data[index_rx_data] = ReadUART1();
            WriteUART1(uart_rx_data[index_rx_data]);
            IFS1bits.U1RXIF = 0;
        }
        if(uart_data_verification())
        {
            //Control LEDs period heartbeat with data received
            handle_heartbeat_command();
        }
    }
}

bool uart_data_verification(void)
{
    if (uart_rx_data[0] == heartbeat_command_control[0] && uart_rx_data[3] == heartbeat_command_control[3]) 
    {
        return true;
    }
    return false;
}

void handle_heartbeat_command(void)
{
    //Parse data
    heartbeat_command_control [1] = (uart_rx_data[1]-48);
    uint8_t index_led = heartbeat_command_control[1]-1;
    
    //1 (LED2 - Green) - 2 (LED3 - Yellow)
    if(index_led == 1 | index_led == 2)
    {
        //Parse data
        heartbeat_command_control[2] = (uart_rx_data[2]-48);
        
        if(heartbeat_command_control[2]>=0 && heartbeat_command_control[2]<=9)
        {
            LEDs_heartbeat_period[index_led] = heartbeat_command_control[2]*100;
        }
    }
}